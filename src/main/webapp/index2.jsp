<%@ page import="java.time.LocalTime" %><%--
  Created by IntelliJ IDEA.
  User: marekdybusc
  Date: 19.01.2018
  Time: 19:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello, World!</title>
</head>
<body>
<h1><% out.print("Hello, World!"); %></h1>
<div>
    <%
        out.print("<table>");
        int i = 100;
        long start = LocalTime.now().toNanoOfDay();
        int a = 1;
        int b = 1;
        for (int j = 1; j <= i; j++) {
            if (j % 5 == 1) {
                out.print("<tr>");
            }
            if (j == 1 || j == 2) {
                out.print("<td>");
                out.print("1");
                out.print("</td>");
            }
            if (j >= 3) {
                int c = a + b;
                out.print("<td>");
                out.print(c);
                out.print("</td>");
                a = b;
                b = c;
            }
            if (j % 5 == 0) {
                out.print("</tr>");
            }
        }

        out.println("<br />");
        out.println(request.getRequestURL() + String.valueOf(response.getStatus()));
        long duration2 = (LocalTime.now().toNanoOfDay()) - start;
        for (int j = 0; j < 5; j++) {
        }

        out.print("</table>");
        out.println("duration = " + duration2);
        out.print("<h1>");
        out.print("name" + request.getParameter("name"));
        out.print("</h1>");


    %>
</div>
<a href="index.jsp">go to the other index</a>
</body>
</html>
